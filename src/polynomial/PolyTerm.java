package polynomial;

/**
 * represents an indivdual term of a polynomial, such as 3x2, where 3 
 * is c, the coefficient, and 2 is e, the exponent!
 * @author sendrae
 * @author martinml
 */
@SuppressWarnings("rawtypes")
public class PolyTerm implements Comparable 
{
	// coefficient
	private int c;
	// exponent
	private int e;
	
	/**
	 * constructs the polyterm with c as coefficient, e as exponent
	 * @param c
	 * @param e
	 */
	public PolyTerm(int c, int e)
	{
		this.c = c;
		this.e = e;
	}
	
	/**
	 * gets the exponent 
	 * @return integer exponent 
	 */
	public int getExponent()
	{
		return e;
	}
	
	/**
	 * gets the coefficient
	 * @return integer coefficient
	 */
	public int getCoefficient()
	{
		return c;
	}
	
	/**
	 * sets the coefficient, helpful when subtracting 
	 * @param c
	 */
	public void setCoefficient(int c)
	{
		this.c = c;
	}

	/**
	 * override the compare method such that these can be sorted by exponent
	 */
	@Override
	public int compareTo(Object arg0)
	{
		if(!(arg0 instanceof PolyTerm))
			throw new UnsupportedOperationException("Cannot compare non-polyterm types.");
		
		PolyTerm p0 = (PolyTerm) arg0;
		if(e < p0.e)
			return -1;
		else if(e > p0.e)
			return 1;
		else
			return 0;
	}
}

/*
 * Names:
 * Evan Sendra
 * Matthew Martin
 * 
 * Problems:
 * We had some difficulty thinking of all the edge cases
 * which would be triggered in tests.  Also, the subtract
 * method seemed complicated until we realized we could negate
 * the subtrahend and add it to the minuend.
 * 
 * Defects found:
 * The tests failed at first, because we realized our toString
 * method was ordering terms backwards.  Once we saw exactly how
 * our tests were failing, we realized the fix would be as 
 * easy as reversing the iteration of a for loop!
 * 
 * Experience creating the lab:
 * Test-driven development is quite hard when you have to 
 * write tests and the code which will be tested.  It's hard, because
 * you must first think of all edge cases and corresponding tests, then 
 * you must look at the problem from the standpoint of having 
 * to write code which passes all those tests.  This was a 
 * bit overwhelming at first, though it was definitely 
 * helpful in the end.
 */