package polynomial;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * A java-representation of a Polynomial, which is composed of PolyTerms 
 * @author sendrae
 * @author martinml
 */
public class Polynomial
{
	private List<PolyTerm> polynomial;

	/**
	 * constructs a polynomial, removing zero coefficient terms and negative exponent terms, but
	 * throws exception for duplicate exponent terms
	 * @param polynomial a List of PolyTerm objects
	 * @throws NumberFormatException if there are duplicate exponent terms in the List 
	 */
	public Polynomial(List<PolyTerm> polynomial) throws NumberFormatException
	{
		// int[] history = new int[]{}; // terms we've already seen
		List<Integer> history = new ArrayList<Integer>();
		// remove negative exponents and zero terms
		for(int k=0; k < polynomial.size(); k++)
		{
			
			// throw exception if duplicate terms
			for(int i = 0; i < history.size(); i++)
			{
				if(polynomial.get(k).getExponent() == history.get(i))
				{
					throw new NumberFormatException("Cannot pass in list with "
							+ "more than 1 term of same exponent.");
				}
			}
			history.add(polynomial.get(k).getExponent());
			
			if(polynomial.get(k).getCoefficient() == 0)
				polynomial.remove(polynomial.get(k));
			else if(polynomial.get(k).getExponent() < 0)
				polynomial.remove(polynomial.get(k));
		}
		Collections.sort(polynomial);
		this.polynomial = polynomial;
	}

	/**
	 * goes through the polynomial to which this polynomial is being added, instantiating a queue
	 * data structure to leverage the fact that exponents are sorted and can therefore be "bubbled up"
	 * the queue to meet the corresponding term of equal order, if it exists
	 * @author Evan Sendra
	 * @param a the polynomial to which this polynomial is added
	 * @return the sum Polynomial
	 */
	public Polynomial add(Polynomial a)
	{
		Polynomial sum = null;
		// if either is empty, just return the other one
		if(polynomial.isEmpty())
			sum = a;
		else if(a.getTerms().isEmpty())
			sum = this;
		else
		{
			// make two stacks, which we'll leverage when adding the terms
			Queue<PolyTerm> p0Terms = new LinkedList<PolyTerm>();
			Queue<PolyTerm> p1Terms = new LinkedList<PolyTerm>();
			
			for(PolyTerm pt: polynomial)
				p0Terms.offer(pt);
			
			for(PolyTerm pt: a.getTerms())
				p1Terms.offer(pt);
			
			List<PolyTerm> newTerms = new ArrayList<PolyTerm>();
			int i;
			for(i=0; !p0Terms.isEmpty() && !p1Terms.isEmpty(); i++)
			{
					int comparison = p0Terms.peek().compareTo(p1Terms.peek());
					if(comparison < 0)
					{
						// this polynomial's exponent is less than a, 
						// so drop down the term from a 
						newTerms.add(p1Terms.poll());
					}
					else if(comparison > 0)
					{
						// this polynomial's exponent is greater than a, 
						// so drop it down to the new set of terms
						newTerms.add(p0Terms.poll());
					}
					else
					{
						// add the coefficients and drop down the term
						int termSum = p0Terms.poll().getCoefficient() + p1Terms.poll().getCoefficient();
						if(termSum != 0)
						{
							PolyTerm curTerm = new PolyTerm(termSum, polynomial.get(i).getExponent());
							newTerms.add(curTerm);
						}
					}
				}
				
				// empty the stacks if the polynomials were different sizes
				if(!p0Terms.isEmpty())
				{
					for(PolyTerm pt: p0Terms)
						newTerms.add(pt);
				}
				else if(!p1Terms.isEmpty())
				{
					for(PolyTerm pt: p1Terms)
						newTerms.add(pt);
				}
				sum = new Polynomial(newTerms);
			}
		return sum;
	}
	
	/**
	 * Used in the subtract method to negate the coefficients of the subtrahend
	 * before being added to the minuend
	 */
	private static void negate(Polynomial b)
	{
		List<PolyTerm> list = b.getTerms();
		for(int i = 0; i< list.size(); i++)
		{
			list.get(i).setCoefficient(0 - list.get(i).getCoefficient());
		}
	}

	/**
	 * Martin's super elegant subtract implementation, which leverages a negated
	 * add :)
	 * @author Martin (Evan authored this comment :))
	 * @param a the subtrahend which is subtracted from the value of the minuend
	 * @return Polynomial which is the difference of this Polynomial - the parameter
	 */
	public Polynomial subtract(Polynomial a)
	{
		negate(a);
		return this.add(a);
	}


	/**
	 * provides the terms of the polynomial 
	 * @return list of the terms in the Polynomial
	 */
	public List<PolyTerm> getTerms()
	{
		return polynomial;
	}

	/**
	 * represents this Polynomial as a string according to the rules as outlined in Dr. Hasker's
	 * web requirements
	 */
	@Override 
	public String toString()
	{
		int coefficient;
		int exponent;
		String result = "";
		if(polynomial.size() == 0)
		{
			result += "0";
		}
		else
		{
			for(int i = polynomial.size()-1; i >= 0; i--)
			{
				coefficient = polynomial.get(i).getCoefficient();
				exponent = polynomial.get(i).getExponent();
				
				String plusSign = i == (polynomial.size()-1) ? "" : " + ";
				
				if(coefficient == 1)
				{
					if(exponent == 0)
						result += plusSign + coefficient;	
					else if(exponent == 1)
						result += plusSign + "x";	
					else 
						result += plusSign + "x" + exponent;	
				}
				else if(coefficient == 0)
				{
					result += "";
				}
				else if(coefficient == -1)
				{
					if(exponent == 0)
						result += plusSign + coefficient;	
					else if(exponent == 1)
						result += plusSign + "-x";	
					else 
						result += plusSign + "-x" + exponent;
				}
				else
				{
					if(exponent == 0)
						result += plusSign + coefficient;	
					else if(exponent == 1)
						result += plusSign + coefficient + "x";	
					else 
						result += plusSign + coefficient + "x" + exponent;
				}
			}
		}
		return result;
	}
}

/*
 * Names:
 * Evan Sendra
 * Matthew Martin
 * 
 * Problems:
 * We had some difficulty thinking of all the edge cases
 * which would be triggered in tests.  Also, the subtract
 * method seemed complicated until we realized we could negate
 * the subtrahend and add it to the minuend.
 * 
 * Defects found:
 * The tests failed at first, because we realized our toString
 * method was ordering terms backwards.  Once we saw exactly how
 * our tests were failing, we realized the fix would be as 
 * easy as reversing the iteration of a for loop!
 * 
 * Experience creating the lab:
 * Test-driven development is quite hard when you have to 
 * write tests and the code which will be tested.  It's hard, because
 * you must first think of all edge cases and corresponding tests, then 
 * you must look at the problem from the standpoint of having 
 * to write code which passes all those tests.  This was a 
 * bit overwhelming at first, though it was definitely 
 * helpful in the end.
 */