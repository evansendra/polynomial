package tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import polynomial.PolyTerm;
import polynomial.Polynomial;

/**
 * Tests the Polynomial class for many edge cases as well as
 * trivial cases
 * @author martinml
 * @author sendrae
 */
public class PolynomialTester
{
	// private static List<PolyTerm> polyTerms;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
		System.out.println("Starting tests ... :)");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
		System.out.println("Finishing tests! :)");
	}

	/**
	 * tests if the Poly class can add polynomials correctly
	 * @author Evan Sendra
	 */
	@Test
	public void testAdd()
	{
		List<PolyTerm> terms0 = new ArrayList<PolyTerm>();
		
		// adds a zero polynomial to the current polynomial, trivial case
		terms0.add(new PolyTerm(3, 2));
		terms0.add(new PolyTerm(-4, 1));
		terms0.add(new PolyTerm(7, 0));
		Polynomial p0 = new Polynomial(terms0);
		p0 = p0.add(new Polynomial(new ArrayList<PolyTerm>())); 
		assertEquals("3x2 + -4x + 7", p0.toString());
		
		// add two actual polynomials, less trivial case
		terms0.clear();
		// builds 3x2 - 4x + 7
		terms0.add(new PolyTerm(3, 2));
		terms0.add(new PolyTerm(-4, 1));
		terms0.add(new PolyTerm(7, 0));
		p0 = new Polynomial(terms0);
		
		List<PolyTerm> terms1 = new ArrayList<PolyTerm>();
		// build 4x3 + 2x2 - x2 - x - 1 which should resolve to 4x3 + x2 - x - 1
		terms1.clear();
		terms1.add(new PolyTerm(4, 3));
		terms1.add(new PolyTerm(2, 2));
		terms1.add(new PolyTerm(-1, 1));
		terms1.add(new PolyTerm(-1, 0));
		Polynomial p1 = new Polynomial(terms1);
		
		// test the sum which should be 4x3 + 4x2 + -5x + 6
		String sum = p0.add(p1).toString();
		assertEquals("4x3 + 5x2 + -5x + 6", sum);
	}
	
	/**
	 *tests to see if a Polynomial of value 0 can be created.
	 *@author martinml 
	 */
	@Test
	public void testZero()
	{
		//martinml
		List<PolyTerm> terms0 = new ArrayList<PolyTerm>();
		terms0.add(new PolyTerm(0,0)); //add an empty PolyTerm, or a Zero term to the list
		Polynomial p = new Polynomial(terms0);//create a polynomial out of the list
		assertEquals("0", p.toString());
		
	}
	
	/**
	 * tests to determine whether or not the Polynomial class is properly subtracting
	 * @author martinml
	 */
	@Test
	public void testSubtract()
	{
		List<PolyTerm> terms0 = new ArrayList<PolyTerm>();
		//martinml
		terms0.clear(); //clear the list
		
		//construct the first polynomial
		terms0.add(new PolyTerm(3,2));
		terms0.add(new PolyTerm(-4, 1));
		terms0.add(new PolyTerm(7, 0));
		Polynomial minuend = new Polynomial(terms0);
		
		
		List<PolyTerm> terms1 = new ArrayList<PolyTerm>();
		//construct the second polynomial
		terms1.add(new PolyTerm(2,2));
		terms1.add(new PolyTerm(3,1));
		terms1.add(new PolyTerm(8,0));
		Polynomial subtrahend = new Polynomial(terms1);
		
		assertEquals("x2 + -7x + -1", minuend.subtract(subtrahend).toString());
		
		
	}
	
	/**
	 * Tests if the polynomial can re-order polynomials correctly
	 * when passed in a collection of polynomials which has exponents
	 * out of order
	 * @author Evan Sendra
	 */
	@Test
	public void testOrder()
	{
		// build the following poly, but exps out of order: 4x500 + 3x2 + 2x - 1
		List<PolyTerm> terms0 = new ArrayList<PolyTerm>();
		terms0.add(new PolyTerm(-1, 0));
		terms0.add(new PolyTerm(4, 500));
		terms0.add(new PolyTerm(3, 2));
		terms0.add(new PolyTerm(2, 1));
		Polynomial p0 = new Polynomial(terms0);
		assertEquals("4x500 + 3x2 + 2x + -1", p0.toString());
	}
	
	/**
	 * tests to see if the Polynomial class correctly hides a zero-value PolyTerm when 
	 * displaying the Polynomial.
	 * @author martinml
	 */
	@Test
	public void testZeroTerm()
	{
		//martinml
		List<PolyTerm> terms0 = new ArrayList<PolyTerm>();
		//create first polynomial
		terms0.add(new PolyTerm(3, 2));
		terms0.add(new PolyTerm(-4, 1));
		terms0.add(new PolyTerm(7, 0));
		Polynomial p = new Polynomial(terms0);
		
		List<PolyTerm> terms1 = new ArrayList<PolyTerm>();
		terms1.clear(); //clear the list for the second Polynomial
		//create a zero value polynomial
		terms1.add(new PolyTerm(0,0)); //add an empty PolyTerm, or a Zero term to the list
		Polynomial z = new Polynomial(terms1);//create a polynomial out of the list
		
		//add the zero term to the first Polynomial, then compare the result to p
		assertEquals("3x2 + -4x + 7", p.add(z).toString());
	}
	
	/**
	 * test if, when a polynomial has coefficients which are one, the
	 * one in front of the term is suppressed
	 * @author Evan Sendra
	 */
	@Test
	public void testOneCoefficient()
	{
		// construct x3 - x2 + 1
		List<PolyTerm> terms0 = new ArrayList<PolyTerm>();
		terms0.add(new PolyTerm(1, 3));
		terms0.add(new PolyTerm(-1, 2));
		terms0.add(new PolyTerm(1, 0));
		Polynomial p0 = new Polynomial(terms0);
		assertEquals("x3 + -x2 + 1", p0.toString()); 
	}
	
	/**
	 * tests to determine whether or not the Polynomial class hides the exponent when 
	 * displaying the Polynomial if the exponent has a value of 1
	 * @author martinml
	 */
	@Test
	public void testExponentOne()
	{
		List<PolyTerm> terms0 = new ArrayList<PolyTerm>();
		//create the Polynomial
		terms0.add(new PolyTerm(4,1));
		Polynomial p = new Polynomial(terms0);
		
		assertEquals("4x", p.toString());
	}

	/**
	 * tests if, when a term has a zero exponent, the term is subsequently
	 * shown as just the coefficient, and added to any other coefficients 
	 * if they exist
	 * @author Evan Sendra
	 */
	@Test
	public void testExponentZero()
	{
		// construct the polynomial x3 - x2 + 5000x0 + 3x0 
		// should resolve to x3 + -x2 + 5003
		List<PolyTerm> terms0 = new ArrayList<PolyTerm>();
		terms0.add(new PolyTerm(1, 3));
		terms0.add(new PolyTerm(-1, 2));
		terms0.add(new PolyTerm(5000, 0));
		Polynomial p0 = new Polynomial(terms0);
		assertEquals("x3 + -x2 + 5000", p0.toString());
	}
	
	/**
	 * tests to determine if the Polynomial class correctly removes any PolyTerm with a
	 * negative exponent
	 * @author martinml
	 */
	@Test
	public void testNegativeExponent()
	{
		List<PolyTerm> terms0 = new ArrayList<PolyTerm>();
		//create the polynomial, purposely including 3x-1
		terms0.add(new PolyTerm(3,2));
		terms0.add(new PolyTerm(-7,0));
		terms0.add(new PolyTerm(3,-1));
		Polynomial p = new Polynomial(terms0);
		//assuming that our code will simply take out the coefficient with the negative exponent:
		assertEquals("3x2 + -7", p.toString());
	}
}

/*
 * Names:
 * Evan Sendra
 * Matthew Martin
 * 
 * Problems:
 * We had some difficulty thinking of all the edge cases
 * which would be triggered in tests.  Also, the subtract
 * method seemed complicated until we realized we could negate
 * the subtrahend and add it to the minuend.
 * 
 * Defects found:
 * The tests failed at first, because we realized our toString
 * method was ordering terms backwards.  Once we saw exactly how
 * our tests were failing, we realized the fix would be as 
 * easy as reversing the iteration of a for loop!
 * 
 * Experience creating the lab:
 * Test-driven development is quite hard when you have to 
 * write tests and the code which will be tested.  It's hard, because
 * you must first think of all edge cases and corresponding tests, then 
 * you must look at the problem from the standpoint of having 
 * to write code which passes all those tests.  This was a 
 * bit overwhelming at first, though it was definitely 
 * helpful in the end.
 */